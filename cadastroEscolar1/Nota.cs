﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadastroEscolar
{
    public class Nota
    {
        private Aluno aluno;
        private Prova prova;
        private Double nota;

        public Double getNota()
        {
            return nota;
        }

        public void setNota(Double nota)
        {
            this.nota = nota;
        }
        public Aluno getAluno()
        {
            return aluno;
        }
        public void setAluno(Aluno aluno)
        {
            this.aluno = aluno;
        }
        public Prova getProva()
        {
            return prova;
        }
        public void setProva(Prova prova)
        {
            this.prova = prova;
        }
       
    }
}
