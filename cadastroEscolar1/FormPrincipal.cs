﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
    public partial class FormPrincipal : Form
    {
        List<Aluno> alunos;
        List<Prova> provas;
        List<Nota> notas;

        public FormPrincipal()
        {
            InitializeComponent();
            alunos = new List<Aluno>();
            provas = new List<Prova>();
            notas = new List<Nota>();
            Conexao c = new Conexao();
            c.conecta();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Deseja finalizar a aplicação?", "Confirmação", MessageBoxButtons.YesNo))
            {
                this.Close();
            }
        }

        private void novoAlunoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormaAluno FormaAluno = new FormaAluno(alunos);
            FormaAluno.Show();
        }

        private void alunoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void novaProvaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormProva FormProva = new FormProva(provas);
            FormProva.Show();
        }

        private void notaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void novaNotaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormNota FormNota = new FormNota (alunos,provas,notas);
            FormNota.Show();
        }

        private void provaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistoricoBusca HistoricoBusca = new HistoricoBusca( notas);
            HistoricoBusca.Show();
        }
    }
}
