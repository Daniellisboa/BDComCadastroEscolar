﻿namespace cadastroEscolar
{
    partial class FormNota
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblAluno2 = new System.Windows.Forms.Label();
			this.lblProva2 = new System.Windows.Forms.Label();
			this.lblNota = new System.Windows.Forms.Label();
			this.txtNota = new System.Windows.Forms.TextBox();
			this.txtNovaProva2 = new System.Windows.Forms.TextBox();
			this.txtNovoAluno1 = new System.Windows.Forms.TextBox();
			this.btSalvarNota = new System.Windows.Forms.Button();
			this.btBuscarProva = new System.Windows.Forms.Button();
			this.btBuscarAluno = new System.Windows.Forms.Button();
			this.btCancelar2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblAluno2
			// 
			this.lblAluno2.AutoSize = true;
			this.lblAluno2.Location = new System.Drawing.Point(45, 39);
			this.lblAluno2.Name = "lblAluno2";
			this.lblAluno2.Size = new System.Drawing.Size(86, 13);
			this.lblAluno2.TabIndex = 0;
			this.lblAluno2.Text = "Nome do Aluno :";
			// 
			// lblProva2
			// 
			this.lblProva2.AutoSize = true;
			this.lblProva2.Location = new System.Drawing.Point(45, 80);
			this.lblProva2.Name = "lblProva2";
			this.lblProva2.Size = new System.Drawing.Size(87, 13);
			this.lblProva2.TabIndex = 1;
			this.lblProva2.Text = "Nome da Prova :";
			// 
			// lblNota
			// 
			this.lblNota.AutoSize = true;
			this.lblNota.Location = new System.Drawing.Point(45, 156);
			this.lblNota.Name = "lblNota";
			this.lblNota.Size = new System.Drawing.Size(36, 13);
			this.lblNota.TabIndex = 2;
			this.lblNota.Text = "Nota :";
			// 
			// txtNota
			// 
			this.txtNota.Location = new System.Drawing.Point(100, 153);
			this.txtNota.Name = "txtNota";
			this.txtNota.Size = new System.Drawing.Size(126, 20);
			this.txtNota.TabIndex = 3;
			this.txtNota.TextChanged += new System.EventHandler(this.txtNota_TextChanged);
			// 
			// txtNovaProva2
			// 
			this.txtNovaProva2.Enabled = false;
			this.txtNovaProva2.Location = new System.Drawing.Point(136, 80);
			this.txtNovaProva2.Name = "txtNovaProva2";
			this.txtNovaProva2.Size = new System.Drawing.Size(299, 20);
			this.txtNovaProva2.TabIndex = 4;
			this.txtNovaProva2.TextChanged += new System.EventHandler(this.txtNovaProva2_TextChanged);
			// 
			// txtNovoAluno1
			// 
			this.txtNovoAluno1.Enabled = false;
			this.txtNovoAluno1.Location = new System.Drawing.Point(136, 36);
			this.txtNovoAluno1.Name = "txtNovoAluno1";
			this.txtNovoAluno1.Size = new System.Drawing.Size(299, 20);
			this.txtNovoAluno1.TabIndex = 5;
			this.txtNovoAluno1.TextChanged += new System.EventHandler(this.txtNovoAluno1_TextChanged);
			// 
			// btSalvarNota
			// 
			this.btSalvarNota.Location = new System.Drawing.Point(136, 223);
			this.btSalvarNota.Name = "btSalvarNota";
			this.btSalvarNota.Size = new System.Drawing.Size(75, 23);
			this.btSalvarNota.TabIndex = 6;
			this.btSalvarNota.Text = "Salvar";
			this.btSalvarNota.UseVisualStyleBackColor = true;
			this.btSalvarNota.Click += new System.EventHandler(this.btSalvarNota_Click);
			// 
			// btBuscarProva
			// 
			this.btBuscarProva.Location = new System.Drawing.Point(441, 78);
			this.btBuscarProva.Name = "btBuscarProva";
			this.btBuscarProva.Size = new System.Drawing.Size(35, 23);
			this.btBuscarProva.TabIndex = 7;
			this.btBuscarProva.Text = "...";
			this.btBuscarProva.UseVisualStyleBackColor = true;
			this.btBuscarProva.Click += new System.EventHandler(this.btBuscarProva_Click);
			// 
			// btBuscarAluno
			// 
			this.btBuscarAluno.Location = new System.Drawing.Point(441, 36);
			this.btBuscarAluno.Name = "btBuscarAluno";
			this.btBuscarAluno.Size = new System.Drawing.Size(35, 23);
			this.btBuscarAluno.TabIndex = 8;
			this.btBuscarAluno.Text = "...";
			this.btBuscarAluno.UseVisualStyleBackColor = true;
			this.btBuscarAluno.Click += new System.EventHandler(this.btBuscarAluno_Click);
			// 
			// btCancelar2
			// 
			this.btCancelar2.Location = new System.Drawing.Point(253, 223);
			this.btCancelar2.Name = "btCancelar2";
			this.btCancelar2.Size = new System.Drawing.Size(75, 23);
			this.btCancelar2.TabIndex = 9;
			this.btCancelar2.Text = "Cancelar";
			this.btCancelar2.UseVisualStyleBackColor = true;
			this.btCancelar2.Click += new System.EventHandler(this.btCancelar2_Click);
			// 
			// FormNota
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(565, 287);
			this.Controls.Add(this.btCancelar2);
			this.Controls.Add(this.btBuscarAluno);
			this.Controls.Add(this.btBuscarProva);
			this.Controls.Add(this.btSalvarNota);
			this.Controls.Add(this.txtNovoAluno1);
			this.Controls.Add(this.txtNovaProva2);
			this.Controls.Add(this.txtNota);
			this.Controls.Add(this.lblNota);
			this.Controls.Add(this.lblProva2);
			this.Controls.Add(this.lblAluno2);
			this.Name = "FormNota";
			this.Text = "FormNota";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAluno2;
        private System.Windows.Forms.Label lblProva2;
        private System.Windows.Forms.Label lblNota;
        private System.Windows.Forms.TextBox txtNota;
        public System.Windows.Forms.TextBox txtNovaProva2;
        public System.Windows.Forms.TextBox txtNovoAluno1;
        private System.Windows.Forms.Button btSalvarNota;
        private System.Windows.Forms.Button btBuscarProva;
        private System.Windows.Forms.Button btBuscarAluno;
        private System.Windows.Forms.Button btCancelar2;
    }
}