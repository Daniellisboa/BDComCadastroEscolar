﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadastroEscolar
{
    class ProvaBD
    {
        public int inserirProva(Prova prova)
        {
            Conexao c = new Conexao();
            // NpgsqlConnection isso e uma classe que vai ser sempre usado para fazer os insert e vai iniciar uma concexao com o c.conecta da classe conexao
            NpgsqlConnection conn = c.conecta();
            NpgsqlCommand cmd = new NpgsqlCommand("insert into prova(descricao,dataRealizacao ) values('" + prova.getDescricao() + "','" +
                prova.getDataRealizacaoe() + "' )", conn);

            int result = cmd.ExecuteNonQuery();
            // desconecta e pra fechar a conecção da classe conexao;
            c.desconetcta();

            return result;
        }

        public List<Prova> buscarProvaPorDescricao(String descricao)
        {
            Conexao c = new Conexao();
            NpgsqlConnection conn = c.conecta();
            NpgsqlCommand cmd = new NpgsqlCommand("select * from prova  where descricao like '%" + descricao + "%'", conn);
            NpgsqlDataReader dRead = cmd.ExecuteReader();
            List<Prova> provas = new List<Prova>();

            while (dRead.Read())
            {
                Prova p = new Prova();
                p.setIdProva(Convert.ToInt32(dRead[0]));
                p.setDescricao(Convert.ToString(dRead[2]));
                p.setDataRealizacao(Convert.ToDateTime(dRead[1]));
                
                provas.Add(p);
            }

            c.desconetcta();
            return provas;
        }

        public List<Prova> buscarProva(String descricao)
        {
            Conexao c = new Conexao();
            NpgsqlConnection conn = c.conecta();
            NpgsqlCommand cmd = new NpgsqlCommand("select * from prova ", conn);
            NpgsqlDataReader dRead = cmd.ExecuteReader();
            List<Prova> provas = new List<Prova>();

            while (dRead.Read())
            {
                Prova p = new Prova();
                p.setIdProva(Convert.ToInt32(dRead[0]));
                p.setDescricao(Convert.ToString(dRead[1]));
                p.setDataRealizacao(Convert.ToDateTime(dRead[2]));

                provas.Add(p);
            }

            c.desconetcta();
            return provas;
        }
    }
}
