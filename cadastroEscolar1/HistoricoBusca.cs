﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
    public partial class HistoricoBusca : Form
    {
        
        List<Nota> notas;


        public HistoricoBusca( List<Nota> notas)
        {
            InitializeComponent();
            
            this.notas = notas;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btHistoricoBusca_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            List<Nota> NotasBusca = new List<Nota>();
			//NotaBD notaBd = new NotaBD();
			//List<Nota> NotasBusca = alunoBd.buscarAlunosPorNome(txtBuscaAluno.Text);

			foreach (Nota n in notas)
            {

                if (n.getAluno().getNome().Contains(textBox1.Text))
                {
                    NotasBusca.Add(n);
                }
            }
            if (NotasBusca.Count() == 0)
            {

                dataGridView1.Visible = false;
            }
            else
            {
                dataGridView1.Visible = true;
               
            }

        
            foreach (Nota n in NotasBusca)
            {

                dataGridView1.Rows.Add(n.getAluno().getMatricula(), n.getAluno().getNome(), n.getProva().getDescricao(),n.getNota());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

