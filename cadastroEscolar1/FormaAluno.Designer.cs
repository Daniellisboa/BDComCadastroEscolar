﻿namespace cadastroEscolar
{
    partial class FormaAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblNomeAluno = new System.Windows.Forms.Label();
			this.lblMatricula = new System.Windows.Forms.Label();
			this.lblDataNascimento = new System.Windows.Forms.Label();
			this.lblDataMatricula = new System.Windows.Forms.Label();
			this.txtNomeAluno = new System.Windows.Forms.TextBox();
			this.txtMatricula = new System.Windows.Forms.TextBox();
			this.btSalvarAluno = new System.Windows.Forms.Button();
			this.btCancelar = new System.Windows.Forms.Button();
			this.txtDataNascimento = new System.Windows.Forms.DateTimePicker();
			this.txtDataMatricula = new System.Windows.Forms.DateTimePicker();
			this.SuspendLayout();
			// 
			// lblNomeAluno
			// 
			this.lblNomeAluno.AutoSize = true;
			this.lblNomeAluno.Location = new System.Drawing.Point(44, 70);
			this.lblNomeAluno.Name = "lblNomeAluno";
			this.lblNomeAluno.Size = new System.Drawing.Size(41, 13);
			this.lblNomeAluno.TabIndex = 0;
			this.lblNomeAluno.Text = "Nome :";
			// 
			// lblMatricula
			// 
			this.lblMatricula.AutoSize = true;
			this.lblMatricula.Location = new System.Drawing.Point(44, 129);
			this.lblMatricula.Name = "lblMatricula";
			this.lblMatricula.Size = new System.Drawing.Size(56, 13);
			this.lblMatricula.TabIndex = 1;
			this.lblMatricula.Text = "Matricula :";
			// 
			// lblDataNascimento
			// 
			this.lblDataNascimento.AutoSize = true;
			this.lblDataNascimento.Location = new System.Drawing.Point(44, 175);
			this.lblDataNascimento.Name = "lblDataNascimento";
			this.lblDataNascimento.Size = new System.Drawing.Size(110, 13);
			this.lblDataNascimento.TabIndex = 2;
			this.lblDataNascimento.Text = "Data de Nascimento :";
			// 
			// lblDataMatricula
			// 
			this.lblDataMatricula.AutoSize = true;
			this.lblDataMatricula.Location = new System.Drawing.Point(44, 228);
			this.lblDataMatricula.Name = "lblDataMatricula";
			this.lblDataMatricula.Size = new System.Drawing.Size(97, 13);
			this.lblDataMatricula.TabIndex = 3;
			this.lblDataMatricula.Text = "Data da Matricula :";
			// 
			// txtNomeAluno
			// 
			this.txtNomeAluno.Location = new System.Drawing.Point(105, 70);
			this.txtNomeAluno.Name = "txtNomeAluno";
			this.txtNomeAluno.Size = new System.Drawing.Size(365, 20);
			this.txtNomeAluno.TabIndex = 4;
			this.txtNomeAluno.TextChanged += new System.EventHandler(this.txtNomeAluno_TextChanged);
			// 
			// txtMatricula
			// 
			this.txtMatricula.Location = new System.Drawing.Point(105, 129);
			this.txtMatricula.Name = "txtMatricula";
			this.txtMatricula.Size = new System.Drawing.Size(203, 20);
			this.txtMatricula.TabIndex = 6;
			this.txtMatricula.TextChanged += new System.EventHandler(this.txtMatricula_TextChanged);
			// 
			// btSalvarAluno
			// 
			this.btSalvarAluno.Location = new System.Drawing.Point(164, 273);
			this.btSalvarAluno.Name = "btSalvarAluno";
			this.btSalvarAluno.Size = new System.Drawing.Size(75, 23);
			this.btSalvarAluno.TabIndex = 8;
			this.btSalvarAluno.Text = "Salvar";
			this.btSalvarAluno.UseVisualStyleBackColor = true;
			this.btSalvarAluno.Click += new System.EventHandler(this.btSalvarAluno_Click);
			// 
			// btCancelar
			// 
			this.btCancelar.Location = new System.Drawing.Point(274, 273);
			this.btCancelar.Name = "btCancelar";
			this.btCancelar.Size = new System.Drawing.Size(75, 23);
			this.btCancelar.TabIndex = 9;
			this.btCancelar.Text = "Cancelar";
			this.btCancelar.UseVisualStyleBackColor = true;
			this.btCancelar.Click += new System.EventHandler(this.btCancelar_Click);
			// 
			// txtDataNascimento
			// 
			this.txtDataNascimento.Location = new System.Drawing.Point(160, 169);
			this.txtDataNascimento.Name = "txtDataNascimento";
			this.txtDataNascimento.Size = new System.Drawing.Size(200, 20);
			this.txtDataNascimento.TabIndex = 10;
			this.txtDataNascimento.ValueChanged += new System.EventHandler(this.txtDataNascimento_ValueChanged);
			// 
			// txtDataMatricula
			// 
			this.txtDataMatricula.Location = new System.Drawing.Point(160, 221);
			this.txtDataMatricula.Name = "txtDataMatricula";
			this.txtDataMatricula.Size = new System.Drawing.Size(200, 20);
			this.txtDataMatricula.TabIndex = 11;
			// 
			// FormaAluno
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(566, 322);
			this.Controls.Add(this.txtDataMatricula);
			this.Controls.Add(this.txtDataNascimento);
			this.Controls.Add(this.btCancelar);
			this.Controls.Add(this.btSalvarAluno);
			this.Controls.Add(this.txtMatricula);
			this.Controls.Add(this.txtNomeAluno);
			this.Controls.Add(this.lblDataMatricula);
			this.Controls.Add(this.lblDataNascimento);
			this.Controls.Add(this.lblMatricula);
			this.Controls.Add(this.lblNomeAluno);
			this.Name = "FormaAluno";
			this.Text = "FormaAluno";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomeAluno;
        private System.Windows.Forms.Label lblMatricula;
        private System.Windows.Forms.Label lblDataNascimento;
        private System.Windows.Forms.Label lblDataMatricula;
        public System.Windows.Forms.TextBox txtNomeAluno;
        private System.Windows.Forms.TextBox txtMatricula;
        private System.Windows.Forms.Button btSalvarAluno;
        private System.Windows.Forms.Button btCancelar;
		private System.Windows.Forms.DateTimePicker txtDataNascimento;
		private System.Windows.Forms.DateTimePicker txtDataMatricula;
	}
}