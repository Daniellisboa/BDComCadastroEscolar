﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
	class Conexao
	{
		NpgsqlConnection conn;
        // para fazer a conexao com o banco de dados tem q substiuir password e database pela senha e nome do projeto do banco ;
		String connectionString = "Server=localhost; Port = 5432; User Id=postgres;" + "Password=lab;Database=CadastroEscolar;";
		//String connectionString = "Server=localhost; Port = 5432; User Id=postgres;" + "Password= ??????;Database=?????;";
		//String connectionString = "Server=localhost; Port = 5432; User Id=postgres;" + "Password=lab;Database=BDSistemaEscola;";

		public NpgsqlConnection conecta()
		{
			// Estabelece ligações a banco de dados

			try
			{
				conn = new NpgsqlConnection(connectionString);

				conn.Open();
				MessageBox.Show("sucesso");

				return conn;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Erro de Ligação");
				return null;
			}
			
		}

		public void desconetcta()
		{
			conn.Close();
		}
	}
}
