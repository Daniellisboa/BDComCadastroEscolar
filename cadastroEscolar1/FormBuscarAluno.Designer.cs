﻿namespace cadastroEscolar
{
    partial class FormBuscarAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAluno2 = new System.Windows.Forms.Label();
            this.txtBuscaAluno = new System.Windows.Forms.TextBox();
            this.gridResultadoBuscaAluno = new System.Windows.Forms.DataGridView();
            this.Aluno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridResultadoBuscaAluno)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAluno2
            // 
            this.lblAluno2.AutoSize = true;
            this.lblAluno2.Location = new System.Drawing.Point(52, 55);
            this.lblAluno2.Name = "lblAluno2";
            this.lblAluno2.Size = new System.Drawing.Size(40, 13);
            this.lblAluno2.TabIndex = 1;
            this.lblAluno2.Text = "Aluno :";
            // 
            // txtBuscaAluno
            // 
            this.txtBuscaAluno.Location = new System.Drawing.Point(98, 55);
            this.txtBuscaAluno.Name = "txtBuscaAluno";
            this.txtBuscaAluno.Size = new System.Drawing.Size(447, 20);
            this.txtBuscaAluno.TabIndex = 2;
            this.txtBuscaAluno.TextChanged += new System.EventHandler(this.txtBuscaAluno_TextChanged);
            // 
            // gridResultadoBuscaAluno
            // 
            this.gridResultadoBuscaAluno.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.gridResultadoBuscaAluno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridResultadoBuscaAluno.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Aluno});
            this.gridResultadoBuscaAluno.GridColor = System.Drawing.SystemColors.HotTrack;
            this.gridResultadoBuscaAluno.Location = new System.Drawing.Point(55, 112);
            this.gridResultadoBuscaAluno.Name = "gridResultadoBuscaAluno";
            this.gridResultadoBuscaAluno.Size = new System.Drawing.Size(501, 141);
            this.gridResultadoBuscaAluno.TabIndex = 3;
            this.gridResultadoBuscaAluno.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridResultadoBuscaAluno_CellContentClick);
            // 
            // Aluno
            // 
            this.Aluno.FillWeight = 500F;
            this.Aluno.HeaderText = "Aluno";
            this.Aluno.Name = "Aluno";
            this.Aluno.Width = 500;
            // 
            // FormBuscarAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(581, 334);
            this.Controls.Add(this.gridResultadoBuscaAluno);
            this.Controls.Add(this.txtBuscaAluno);
            this.Controls.Add(this.lblAluno2);
            this.Name = "FormBuscarAluno";
            this.Text = "FormBuscarAluno";
            ((System.ComponentModel.ISupportInitialize)(this.gridResultadoBuscaAluno)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAluno2;
        public System.Windows.Forms.TextBox txtBuscaAluno;
        private System.Windows.Forms.DataGridView gridResultadoBuscaAluno;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aluno;
    }
}