﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
    public partial class FormNota : Form
    {
        List<Aluno> alunos;              
        List<Prova> provas;
        Prova novaProva = new Prova();
        Aluno novoAluno = new Aluno();
       List<Nota> notas;
        public FormNota(List<Aluno> alunos, List<Prova> provas, List<Nota> notas)
        {
            InitializeComponent();
            this.alunos = alunos;
           this.notas = notas;
            this.provas = provas;
        }

        private void btSalvarNota_Click(object sender, EventArgs e)
        {
            Nota n  = new Nota();
            double x = Convert.ToDouble(txtNota.Text);

            if (x < 0 || x > 10)
            {
                MessageBox.Show("Nota invalida", "Erro", MessageBoxButtons.OK);
                txtNota.Text = "";
            }
            else 
            {
                n.setProva(novaProva);
                n.setAluno(novoAluno);
                n.setNota(x);
                //notas.Add(n);

                NotaBd notaBD = new NotaBd();

                int result = notaBD.inserirNota(n);
                if (result == 0)
                {
                    MessageBox.Show("Erro ao cadastrar prova");
                }
                else
                {
                    MessageBox.Show("Nota Salva com sucesso", "Sucesso", MessageBoxButtons.OK);
                    LimparCampos();
                }

            }

            

        }

        public void LimparCampos()
        {
            txtNovoAluno1.Text = "";
            txtNovaProva2.Text = "";
            txtNota.Text = "";
           
        }

        private void txtNovoAluno1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btBuscarAluno_Click(object sender, EventArgs e)
        {
            FormBuscarAluno formBusca = new FormBuscarAluno(novoAluno, alunos, this);
            formBusca.Show();
        }

        private void txtNota_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNovaProva2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btBuscarProva_Click(object sender, EventArgs e)
        {
            FormBuscarProva formBusca = new FormBuscarProva(novaProva, provas, this);
            formBusca.Show();
        }

        private void btCancelar2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
