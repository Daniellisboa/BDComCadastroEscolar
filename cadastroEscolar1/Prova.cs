﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadastroEscolar
{
    public class Prova
    {
        private int idProva;
        private String descricao;
        public DateTime dataRealizacao;

        public int getIdProva()
        {
            return idProva;
        }
        public void setIdProva(int id)
        {
            this.idProva = id;
        }

        public String getDescricao()
        {
            return descricao;
        }
        public void setDescricao(String descricao)
        {
            this.descricao = descricao;
        }
        public DateTime getDataRealizacaoe()
        {
            return dataRealizacao;
        }
        public void setDataRealizacao(DateTime dataRealizacao)
        {
            this.dataRealizacao = dataRealizacao;
        }
    }
}
