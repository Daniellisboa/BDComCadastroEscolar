﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
    public partial class FormBuscarAluno : Form
    {
        List<Aluno> alunos;
        FormNota novaNota;
        Aluno novoAluno;
        public FormBuscarAluno(Aluno novoAluno, List<Aluno> alunos, FormNota formPai)
        {
            InitializeComponent();
            this.alunos = alunos;
            novaNota = formPai;
            this.novoAluno = novoAluno;
            gridResultadoBuscaAluno.ReadOnly = true;
        }

        private void gridResultadoBuscaAluno_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            novaNota.txtNovoAluno1.Text = gridResultadoBuscaAluno.CurrentRow.Cells[0].Value.ToString();
            novoAluno.setNome(novaNota.txtNovoAluno1.Text);

            
            this.Close();
        }

        private void txtBuscaAluno_TextChanged(object sender, EventArgs e)
        {
            gridResultadoBuscaAluno.Rows.Clear();
            AlunoBD alunoBd = new AlunoBD();
            List<Aluno> alunosBusca = alunoBd.buscarAlunosPorNome(txtBuscaAluno.Text);

            gridResultadoBuscaAluno.Visible = true;


            foreach (Aluno c in alunos)
            {
                if (c.getNome().Contains(txtBuscaAluno.Text))
                {
                    alunosBusca.Add(c);
                }
            }

            foreach (Aluno c in alunosBusca)
            {

                gridResultadoBuscaAluno.Rows.Add(c.getNome());
            }
        }
    }
}
