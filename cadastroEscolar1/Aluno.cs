﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadastroEscolar
{
    public class Aluno
    {
        private int idAluno;
        private String nome;
        private String matricula;
        public DateTime dataNascimento;
        public DateTime dataMatricula;

        public int getIdAluno()
        {
            return idAluno;
        }
        public void setIdAluno(int id)
        {
            this.idAluno = id;
        }

        public String getNome()
        {
            return nome;
        }
        public void setNome(String nome)
        {
            this.nome = nome;
        }
        public String getMatricula()
        {
            return matricula;
        }
        public void setMatricula(String matricula)
        {
            this.matricula = matricula;
        }
        public DateTime getDataNascimento()
        {
            return dataNascimento;
        }
        public void setDataNascimento(DateTime dataNascimento)
        {
            this.dataNascimento = dataNascimento;
        }
        public DateTime getDatamatriculao()
        {
            return dataMatricula;
        }
        public void setDataMatricula(DateTime dataMatricula)
        {
            this.dataMatricula = dataMatricula;
        }

    }
}
