﻿namespace cadastroEscolar
{
    partial class FormProva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblDescricao = new System.Windows.Forms.Label();
			this.lblDataRealizacao = new System.Windows.Forms.Label();
			this.txtDescricao = new System.Windows.Forms.TextBox();
			this.btSalvarProva = new System.Windows.Forms.Button();
			this.btCancelar3 = new System.Windows.Forms.Button();
			this.txtDataRealizacao = new System.Windows.Forms.DateTimePicker();
			this.SuspendLayout();
			// 
			// lblDescricao
			// 
			this.lblDescricao.AutoSize = true;
			this.lblDescricao.Location = new System.Drawing.Point(41, 66);
			this.lblDescricao.Name = "lblDescricao";
			this.lblDescricao.Size = new System.Drawing.Size(61, 13);
			this.lblDescricao.TabIndex = 0;
			this.lblDescricao.Text = "Descrição :";
			// 
			// lblDataRealizacao
			// 
			this.lblDataRealizacao.AutoSize = true;
			this.lblDataRealizacao.Location = new System.Drawing.Point(44, 113);
			this.lblDataRealizacao.Name = "lblDataRealizacao";
			this.lblDataRealizacao.Size = new System.Drawing.Size(92, 13);
			this.lblDataRealizacao.TabIndex = 1;
			this.lblDataRealizacao.Text = "DataRealização : ";
			// 
			// txtDescricao
			// 
			this.txtDescricao.Location = new System.Drawing.Point(126, 66);
			this.txtDescricao.Name = "txtDescricao";
			this.txtDescricao.Size = new System.Drawing.Size(205, 20);
			this.txtDescricao.TabIndex = 2;
			this.txtDescricao.TextChanged += new System.EventHandler(this.txtDescricao_TextChanged);
			// 
			// btSalvarProva
			// 
			this.btSalvarProva.Location = new System.Drawing.Point(142, 202);
			this.btSalvarProva.Name = "btSalvarProva";
			this.btSalvarProva.Size = new System.Drawing.Size(75, 23);
			this.btSalvarProva.TabIndex = 4;
			this.btSalvarProva.Text = "Salvar";
			this.btSalvarProva.UseVisualStyleBackColor = true;
			this.btSalvarProva.Click += new System.EventHandler(this.btSalvarProva_Click);
			// 
			// btCancelar3
			// 
			this.btCancelar3.Location = new System.Drawing.Point(256, 202);
			this.btCancelar3.Name = "btCancelar3";
			this.btCancelar3.Size = new System.Drawing.Size(75, 23);
			this.btCancelar3.TabIndex = 5;
			this.btCancelar3.Text = "Cancelar";
			this.btCancelar3.UseVisualStyleBackColor = true;
			this.btCancelar3.Click += new System.EventHandler(this.btCancelar3_Click);
			// 
			// txtDataRealizacao
			// 
			this.txtDataRealizacao.Location = new System.Drawing.Point(142, 113);
			this.txtDataRealizacao.Name = "txtDataRealizacao";
			this.txtDataRealizacao.Size = new System.Drawing.Size(200, 20);
			this.txtDataRealizacao.TabIndex = 6;
			// 
			// FormProva
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(593, 336);
			this.Controls.Add(this.txtDataRealizacao);
			this.Controls.Add(this.btCancelar3);
			this.Controls.Add(this.btSalvarProva);
			this.Controls.Add(this.txtDescricao);
			this.Controls.Add(this.lblDataRealizacao);
			this.Controls.Add(this.lblDescricao);
			this.Name = "FormProva";
			this.Text = "FormProva";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.Label lblDataRealizacao;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.Button btSalvarProva;
        private System.Windows.Forms.Button btCancelar3;
		private System.Windows.Forms.DateTimePicker txtDataRealizacao;
	}
}