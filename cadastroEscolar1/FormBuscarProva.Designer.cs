﻿namespace cadastroEscolar
{
    partial class FormBuscarProva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblProva2 = new System.Windows.Forms.Label();
            this.txtBuscaProva = new System.Windows.Forms.TextBox();
            this.gridResultadoBuscaProva = new System.Windows.Forms.DataGridView();
            this.Prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridResultadoBuscaProva)).BeginInit();
            this.SuspendLayout();
            // 
            // lblProva2
            // 
            this.lblProva2.AutoSize = true;
            this.lblProva2.Location = new System.Drawing.Point(36, 53);
            this.lblProva2.Name = "lblProva2";
            this.lblProva2.Size = new System.Drawing.Size(41, 13);
            this.lblProva2.TabIndex = 2;
            this.lblProva2.Text = "Prova :";
            // 
            // txtBuscaProva
            // 
            this.txtBuscaProva.Location = new System.Drawing.Point(82, 50);
            this.txtBuscaProva.Name = "txtBuscaProva";
            this.txtBuscaProva.Size = new System.Drawing.Size(478, 20);
            this.txtBuscaProva.TabIndex = 3;
            this.txtBuscaProva.TextChanged += new System.EventHandler(this.txtBuscaProva_TextChanged);
            // 
            // gridResultadoBuscaProva
            // 
            this.gridResultadoBuscaProva.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.gridResultadoBuscaProva.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridResultadoBuscaProva.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Prova});
            this.gridResultadoBuscaProva.GridColor = System.Drawing.SystemColors.HotTrack;
            this.gridResultadoBuscaProva.Location = new System.Drawing.Point(39, 141);
            this.gridResultadoBuscaProva.Name = "gridResultadoBuscaProva";
            this.gridResultadoBuscaProva.Size = new System.Drawing.Size(521, 141);
            this.gridResultadoBuscaProva.TabIndex = 4;
            this.gridResultadoBuscaProva.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridResultadoBuscaProva_CellContentClick);
            // 
            // Prova
            // 
            this.Prova.FillWeight = 500F;
            this.Prova.HeaderText = "Prova";
            this.Prova.Name = "Prova";
            this.Prova.Width = 500;
            // 
            // FormBuscarProva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 330);
            this.Controls.Add(this.gridResultadoBuscaProva);
            this.Controls.Add(this.txtBuscaProva);
            this.Controls.Add(this.lblProva2);
            this.Name = "FormBuscarProva";
            this.Text = "FormBuscarProva";
            ((System.ComponentModel.ISupportInitialize)(this.gridResultadoBuscaProva)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProva2;
        public System.Windows.Forms.TextBox txtBuscaProva;
        private System.Windows.Forms.DataGridView gridResultadoBuscaProva;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prova;
    }
}