﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
    public partial class FormBuscarProva : Form
    {
        List<Prova> provas;
        FormNota novaNota;
        Prova novaProva;
        public FormBuscarProva(Prova novaProva, List<Prova> provas, FormNota formPai)
        {
            InitializeComponent();
            this.provas = provas;
            novaNota = formPai;
            this.novaProva = novaProva;
            gridResultadoBuscaProva.ReadOnly = true;
        }

        private void txtBuscaProva_TextChanged(object sender, EventArgs e)
        {
            gridResultadoBuscaProva.Rows.Clear();
            ProvaBD provaBd = new ProvaBD();
            List<Prova> provasBusca = provaBd.buscarProvaPorDescricao(txtBuscaProva.Text);

            gridResultadoBuscaProva.Visible = true;


            foreach (Prova c in provas)
            {
                if (c.getDescricao().Contains(txtBuscaProva.Text))
                {
                    provasBusca.Add(c);
                }
            }

            foreach (Prova c in provasBusca)
            {

                gridResultadoBuscaProva.Rows.Add(c.getDescricao());
            }
        }

        private void gridResultadoBuscaProva_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            novaNota.txtNovaProva2.Text = gridResultadoBuscaProva.CurrentRow.Cells[0].Value.ToString();
            novaProva.setDescricao(novaNota.txtNovaProva2.Text);

            this.Close();

        }
    }

}

