﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadastroEscolar
{
    public partial class FormProva : Form
    {
        //List<Prova> provas;
        
        public FormProva(List<Prova> provas)
        {
            InitializeComponent();
           // this.provas = provas;
            
        }

        private void txtDescricao_TextChanged(object sender, EventArgs e)
        {

        }

        private void btCancelar3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btSalvarProva_Click(object sender, EventArgs e)
        {
            Prova p = new Prova();
            p.setDescricao(txtDescricao.Text);
            p.setDataRealizacao(txtDataRealizacao.Value);
			// provas.Add(p);

			ProvaBD provaBD = new ProvaBD();
			int result = provaBD.inserirProva(p);
			if (result == 0)
			{
				MessageBox.Show("Erro ao cadastrar prova");
			}
			else
			{
				MessageBox.Show("Prova salva com sucesso", "Sucesso", MessageBoxButtons.OK);
				limparCampos();
			}

			
        }

        public void limparCampos()
        {
            txtDescricao.Text = "";
            txtDataRealizacao.Text = "";
            
        }
    }
}
