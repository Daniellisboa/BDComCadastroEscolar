﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadastroEscolar
{
	class AlunoBD
	{

		public int inserirAluno(Aluno aluno)
		{
			Conexao c = new Conexao();
			// NpgsqlConnection isso e uma classe que vai ser sempre usado para fazer os insert e vai iniciar uma concexao com o c.conecta da classe conexao
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("insert into aluno(nome,matricula,datamatricula,dataNascimento) values('" + aluno.getNome() + "','" + 
				aluno.getMatricula() + "','" + aluno.getDatamatriculao() + "','" + aluno.getDataNascimento() + "' )",conn);

			int result = cmd.ExecuteNonQuery();
			// desconecta e pra fechar a conecção da classe conexao;
			c.desconetcta();

			return result;
		}

        public List<Aluno> buscarAlunosPorNome(String nome)
        {
            Conexao c = new Conexao();
            NpgsqlConnection conn = c.conecta();
            NpgsqlCommand cmd = new NpgsqlCommand("select * from aluno where nome like '%" + nome + "%'", conn);
            NpgsqlDataReader dRead = cmd.ExecuteReader();
            List<Aluno> alunos = new List<Aluno>();
            
            while(dRead.Read())
            {
                Aluno a = new Aluno();
                a.setIdAluno(Convert.ToInt32(dRead[0]));
                a.setNome(Convert.ToString(dRead[1]));
                a.setMatricula(Convert.ToString(dRead[2]));
                a.setDataNascimento(Convert.ToDateTime(dRead[3]));
                a.setDataMatricula(Convert.ToDateTime(dRead[4]));
                alunos.Add(a);
            }

            c.desconetcta();
            return alunos;

            
        }

        public List<Aluno> buscarAlunos(String nome)
        {
            Conexao c = new Conexao();
            NpgsqlConnection conn = c.conecta();
            NpgsqlCommand cmd = new NpgsqlCommand("select * from aluno ", conn);
            NpgsqlDataReader dRead = cmd.ExecuteReader();
            List<Aluno> alunos = new List<Aluno>();

            while (dRead.Read())
            {
                Aluno a = new Aluno();
                a.setIdAluno(Convert.ToInt32(dRead[0]));
                a.setNome(Convert.ToString(dRead[1]));
                a.setMatricula(Convert.ToString(dRead[2]));
                a.setDataNascimento(Convert.ToDateTime(dRead[3]));
                a.setDataMatricula(Convert.ToDateTime(dRead[4]));
                alunos.Add(a);
            }

            c.desconetcta();
            return alunos;


        }
    }
}
